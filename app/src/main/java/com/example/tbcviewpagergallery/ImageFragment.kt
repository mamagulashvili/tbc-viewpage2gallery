package com.example.tbcviewpagergallery

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.tbcviewpagergallery.databinding.FragmentImageBinding
class ImageFragment : Fragment(R.layout.fragment_image) {
    private var _binding: FragmentImageBinding? = null
    private val binding: FragmentImageBinding get() = _binding!!
    val args : ImageFragmentArgs by  navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentImageBinding.bind(view)
        init()
    }
    private fun init(){
        Glide.with(this).load(args.image).into(binding.ivFullScreenImage)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}