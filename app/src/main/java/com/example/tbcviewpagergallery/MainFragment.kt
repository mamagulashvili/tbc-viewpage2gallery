package com.example.tbcviewpagergallery

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.tbcviewpagergallery.databinding.FragmentMainBinding

class MainFragment : Fragment(R.layout.fragment_main) {

    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding get() = _binding!!

    private val imageContainer = mutableListOf<String>()
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentMainBinding.bind(view)
        initViewPager()

    }

    private fun initViewPager() {
        val onImageClickListener = object : OnImageClick {
            override fun onImageClickListener(position: Int) {
                onImageClick(position)
            }

        }
        viewPagerAdapter = ViewPagerAdapter(imageContainer, onImageClickListener)
        binding.vpGallery.adapter = viewPagerAdapter
        if (imageContainer.size == 0){
            setImages()
        }
    }

    private fun onImageClick(position: Int) {
        val url = imageContainer[position]
        val action = MainFragmentDirections.actionMainFragmentToImageFragment(url)
        Navigation.findNavController(binding.root).navigate(action)
    }

    private fun setImages() {
        imageContainer.apply {
            add("https://paininthearsenal.com/wp-content/uploads/getty-images/2017/07/1255767977.jpeg")
            add("https://www.arsenal.com/sites/default/files/styles/player_featured_image_1045x658/public/images/Saka_1045x658_0.jpg?itok=Tx1tc5do")
            add("https://pbs.twimg.com/media/EuNarr7XAAIWU60.jpg")
            add("https://goonertalk.com/wp-content/uploads/2020/05/0_Premier-League-Arsenal-v-Aston-Villa.jpg")
            add("https://d3vlf99qeg6bpx.cloudfront.net/content/uploads/2021/04/15160840/Gabriel-Magalhaes-Arsenal-FC-TEAMtalk.jpg")
            add("http s://images.daznservices.com/di/library/GOAL/43/ea/alexandre-lacazette-arsenal-2019-20_fvy05fqyhivm10oypxv1npaht.jpg?t=-1622901884&quality=60&w=1200&h=800")
            add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTC1OpoX-lvpdyGSm1fyHvPlrUaRczyoMx9MA&usqp=CAU")
        }
        viewPagerAdapter.notifyDataSetChanged()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}