package com.example.tbcviewpagergallery

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcviewpagergallery.databinding.RowImageBinding


class ViewPagerAdapter(
    val imageList: MutableList<String>,
    val clickListener:OnImageClick
) : RecyclerView.Adapter<ViewPagerAdapter.ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            RowImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) = holder.onBind()

    override fun getItemCount(): Int = imageList.size

    inner class ImageViewHolder(val binding: RowImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val image = imageList[adapterPosition]
            itemView.apply {
                Glide.with(this).load(image).into(binding.ivGalleryImage)
            }
            binding.ivGalleryImage.setOnClickListener {
                clickListener.onImageClickListener(adapterPosition)
            }
        }
    }
}